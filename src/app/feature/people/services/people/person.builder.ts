import { Gender } from '@core/feature/people/enums/gender';
import { IAddress } from '@core/feature/people/interfaces/IAddress';
import { IPerson } from '@core/feature/people/interfaces/IPerson';

export class PersonBuilder {
  private _name: { title: string; first: string; last: string } = {
    title: '',
    first: '',
    last: '',
  };
  private _age: number = 0;
  private _gender: Gender = Gender.Male;
  private _address: IAddress = {
    city: '',
    country: '',
    postcode: '',
    state: '',
    street: {
      number: '',
      name: '',
    },
  };
  private _picture: {
    large: string;
    medium: string;
    thumbnail: string;
  } = {
    large: '',
    medium: '',
    thumbnail: '',
  };

  /**
   *
   * @param value
   * @constructor
   */
  public WithName(value: {
    title: string;
    first: string;
    last: string;
  }): PersonBuilder {
    this._name = value;

    return this;
  }

  /**
   *
   * @param value
   * @constructor
   */
  public WithAge(value: number): PersonBuilder {
    this._age = value;

    return this;
  }

  /**
   *
   * @constructor
   */
  public Build(): IPerson {
    return {
      name: this._name,
      age: this._age,
      gender: this._gender,
      address: this._address,
      picture: this._picture,
    };
  }
}
