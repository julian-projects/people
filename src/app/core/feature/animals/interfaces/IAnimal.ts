import { IFood } from './IFood';
import { ISpecies } from './ISpecies';

export interface IAnimal {
  id: string;
  name: string;
  species: ISpecies;
  birthday: number;
  description: string;
  favoriteFood: IFood[];
  createdAt?: number;
  updatedAt?: number;
}
