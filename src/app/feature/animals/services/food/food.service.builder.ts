import { IFood } from '@core/feature/animals/interfaces/IFood';
import { FoodService } from '@feature/animals/services/food/food.service';

export class FoodServiceBuilder {
  private _food: IFood[] = [];
  private _fakeDelay: number = 0;

  /**
   *
   * @param food
   * @constructor
   */
  public WithFood(food: IFood[]): FoodServiceBuilder {
    this._food = food;

    return this;
  }

  /**
   *
   * @param value
   * @constructor
   */
  public WithFakeDelay(value: number): FoodServiceBuilder {
    this._fakeDelay = value;

    return this;
  }

  /**
   *
   * @constructor
   */
  public Build(): FoodService {
    return new FoodService().Init(this._food, this._fakeDelay);
  }
}
