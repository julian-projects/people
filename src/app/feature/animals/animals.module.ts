import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatOptionModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AnimalsRoutingModule } from '@feature/animals/animals-routing.module';
import { AnimalComponent } from '@feature/animals/components/animal/animal.component';
import { AnimalDialogComponent } from '@feature/animals/components/animal-dialog/animal-dialog.component';
import { AnimalFormComponent } from '@feature/animals/components/animal-form/animal-form.component';
import { AnimalDetailComponent } from '@feature/animals/pages/animal-detail/animal-detail.component';
import { AnimalsComponent } from '@feature/animals/pages/animals/animals.component';
import { LayoutDefaultComponent } from '@shared/components/layouts/layout-default/layout-default.component';
import { ListingComponent } from '@shared/components/listing/listing.component';
import { LoadingIndicatorComponent } from '@shared/components/loading-indicator/loading-indicator.component';

@NgModule({
  declarations: [
    AnimalsComponent,
    AnimalDialogComponent,
    AnimalDetailComponent,
    AnimalComponent,
  ],
  imports: [
    CommonModule,
    AnimalsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutDefaultComponent,
    ListingComponent,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatOptionModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatDividerModule,
    MatCardModule,
    AnimalFormComponent,
    MatSnackBarModule,
    LoadingIndicatorComponent,
  ],
})
export class AnimalsModule {}
