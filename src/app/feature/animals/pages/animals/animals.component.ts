import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { IAnimal } from '@core/feature/animals/interfaces/IAnimal';
import { IAnimalDialogData } from '@core/feature/animals/interfaces/IAnimalDialogData';
import { AnimalDialogComponent } from '@feature/animals/components/animal-dialog/animal-dialog.component';
import { AnimalServiceProxy } from '@feature/animals/services/animal/animal.service.proxy';
import { TrackType } from '@shared/services/tracking/track-type';
import { TrackingService } from '@shared/services/tracking/tracking.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-animals',
  templateUrl: './animals.component.html',
  styleUrls: ['./animals.component.scss'],
})
export class AnimalsComponent implements OnInit, OnDestroy {
  public animals$: Observable<IAnimal[]>;

  private dialogCloseSubscription!: Subscription;

  constructor(
    private animalService: AnimalServiceProxy,
    private dialog: MatDialog,
    private trackingService: TrackingService
  ) {
    this.animals$ = this.animalService.getAnimals();
  }

  ngOnInit(): void {
    this.trackingService.track(TrackType.Pageview, { name: 'Animals' });
  }

  ngOnDestroy(): void {
    this.dialogCloseSubscription?.unsubscribe();
  }

  /**
   *
   */
  public openDialog(): void {
    const dialogConfig = new MatDialogConfig();

    // dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.width = '500px';

    dialogConfig.data = {
      id: 1,
      title: 'New Animal',
    };

    const dialogRef = this.dialog.open<
      AnimalDialogComponent,
      IAnimalDialogData
    >(AnimalDialogComponent, dialogConfig);

    // this.dialogCloseSubscription = dialogRef.afterClosed().subscribe(data => {
    // const animal: Animal = {
    //   id: new Date().getTime().toString(),
    //   name: data.name,
    //   age: data.age,
    //   description: data.description,
    //   favoriteFood: [],
    // };
    //
    // this.addAnimal(animal);
    // });
  }
}
