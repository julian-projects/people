import { Injectable } from '@angular/core';
import { ISpecies } from '@core/feature/animals/interfaces/ISpecies';
import { ISpeciesService } from '@core/feature/animals/interfaces/ISpeciesService';
import { ServiceBase } from '@shared/services/service.base';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, debounceTime, delay, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SpeciesService extends ServiceBase implements ISpeciesService {
  private _species$: BehaviorSubject<ISpecies[]> = new BehaviorSubject<
    ISpecies[]
  >([
    {
      id: 'dwkafn21',
      name: 'Ape',
    },
    {
      id: 'wdafawf',
      name: 'Snake',
    },
    {
      id: 'gejlkesf',
      name: 'Human',
    },
    {
      id: 'dwajdklf',
      name: 'Horse',
    },
    {
      id: 'nnjkanda',
      name: 'Cat',
    },
    {
      id: 'gawdwad',
      name: 'Dog',
    },
    {
      id: 'gwawdawdw',
      name: 'Pig',
    },
    {
      id: 'tfesdad',
      name: 'Bird',
    },
  ]);
  private _fakeDelay: number = 350;

  constructor() {
    super();
  }

  /**
   *
   * @param species
   * @param fakeDelay
   * @constructor
   */
  public Init(species: ISpecies[], fakeDelay: number): SpeciesService {
    this._species$.next(species);
    this._fakeDelay = fakeDelay;

    return this;
  }

  getAllSpecies(): Observable<ISpecies[]> {
    return this._species$.pipe(
      debounceTime(500),
      tap(result =>
        result.sort((a, b) => {
          return a.name.localeCompare(b.name);
        })
      ),
      catchError(this.handleError<ISpecies[]>('getAllSpecies'))
    );
  }

  getSpecies(id: string): Observable<ISpecies | undefined> {
    const species = this._species$.value.find(el => el.id === id);

    return of(species).pipe(
      delay(this._fakeDelay),
      tap(value => {
        if (!value) throw new Error('Item not found!');
      }),
      catchError(this.handleError<ISpecies>('getSpecies'))
    );
  }

  addSpecies(species: ISpecies): Observable<ISpecies> {
    return of(species).pipe(
      delay(this._fakeDelay),
      tap(value => {
        this._species$.next([...this._species$.value, species]);
      }),
      catchError(this.handleError<ISpecies>('addSpecies'))
    );
  }

  deleteSpecies(id: string): Observable<ISpecies> {
    const index = this._species$.value.findIndex(el => el.id === id);
    const species = this._species$.value[index];

    this._species$.value.splice(index, 1);

    return of(species).pipe(
      delay(this._fakeDelay),
      tap(value => {
        if (!value) throw new Error('Item not found!');
      }),
      catchError(this.handleError<ISpecies>('getSpecies'))
    );
  }

  updateSpecies(
    id: string,
    updateData: Partial<ISpecies>
  ): Observable<ISpecies> {
    const index = this._species$.value.findIndex(el => el.id === id);
    const old = this._species$.value[index];

    return of(old).pipe(
      delay(this._fakeDelay),
      map(value => {
        if (!value) throw new Error('Item not found!');

        const updated = { ...old, ...updateData };
        this._species$.value[index] = updated;
        return updated;
      }),
      catchError(this.handleError<ISpecies>('getSpecies'))
    );
  }
}
