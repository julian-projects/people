import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnimalDetailComponent } from '@feature/animals/pages/animal-detail/animal-detail.component';
import { AnimalsComponent } from '@feature/animals/pages/animals/animals.component';

const routes: Routes = [
  {
    path: '',
    component: AnimalsComponent,
  },
  {
    path: ':id',
    component: AnimalDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnimalsRoutingModule {}
