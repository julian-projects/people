import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { IPerson } from '@core/feature/people/interfaces/IPerson';
import { PersonComponent } from '@feature/people/components/person/person.component';
import { PersonBuilder } from '@feature/people/services/people/person.builder';
import { TruncatePipe } from '@shared/pipes/truncate.pipe';

describe('PersonComponent', () => {
  let component: PersonComponent;
  let fixture: ComponentFixture<PersonComponent>;

  const personInput: IPerson = new PersonBuilder()
    .WithName({
      title: '',
      first: 'John',
      last: 'Doe',
    })
    .Build();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PersonComponent],
      imports: [MatCardModule, TruncatePipe],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonComponent);
    component = fixture.componentInstance;
    component.person = personInput;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
