import { ISpecies } from '@core/feature/animals/interfaces/ISpecies';
import { SpeciesService } from '@feature/animals/services/species/species.service';

export class SpeciesServiceBuilder {
  private _species: ISpecies[] = [];
  private _fakeDelay: number = 0;

  /**
   *
   * @param species
   * @constructor
   */
  public WithSpecies(species: ISpecies[]): SpeciesServiceBuilder {
    this._species = species;

    return this;
  }

  /**
   *
   * @param value
   * @constructor
   */
  public WithFakeDelay(value: number): SpeciesServiceBuilder {
    this._fakeDelay = value;

    return this;
  }

  /**
   *
   * @constructor
   */
  public Build(): SpeciesService {
    return new SpeciesService().Init(this._species, this._fakeDelay);
  }
}
