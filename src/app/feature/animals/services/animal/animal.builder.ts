import { IAnimal } from '@core/feature/animals/interfaces/IAnimal';
import { IFood } from '@core/feature/animals/interfaces/IFood';
import { ISpecies } from '@core/feature/animals/interfaces/ISpecies';

export class AnimalBuilder {
  private _id: string = '';
  private _name: string = '';
  private _species: ISpecies = {
    id: '',
    name: 'default',
  };
  private _age: number = 0;
  private _description: string = '';
  private _favoriteFood: IFood[] = [];

  /**
   *
   * @param value
   * @constructor
   */
  public WithId(value: string): AnimalBuilder {
    this._id = value;

    return this;
  }

  /**
   *
   * @param value
   * @constructor
   */
  public WithName(value: string = 'default'): AnimalBuilder {
    this._name = value;

    return this;
  }

  /**
   *
   * @param value
   * @constructor
   */
  public WithSpecies(value: ISpecies): AnimalBuilder {
    this._species = value;

    return this;
  }

  /**
   *
   * @param value
   * @constructor
   */
  public WithAge(value: number = 42): AnimalBuilder {
    this._age = value;

    return this;
  }

  /**
   *
   * @param value
   * @constructor
   */
  public WithDescription(value: string = 'default'): AnimalBuilder {
    this._description = value;

    return this;
  }

  /**
   *
   * @param value
   * @constructor
   */
  public WithFavoriteFood(value: IFood[]): AnimalBuilder {
    this._favoriteFood = value;

    return this;
  }

  /**
   *
   * @constructor
   */
  public Build(): IAnimal {
    return {
      id: this._id,
      name: this._name,
      species: this._species,
      birthday: this._age,
      description: this._description,
      favoriteFood: this._favoriteFood,
    };
  }
}
