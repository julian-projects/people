import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { By } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { AnimalsComponent } from '@feature/animals/pages/animals/animals.component';
import { LayoutDefaultComponent } from '@shared/components/layouts/layout-default/layout-default.component';
import { LoadingIndicatorComponent } from '@shared/components/loading-indicator/loading-indicator.component';

describe('AnimalsComponent', () => {
  let component: AnimalsComponent;
  let fixture: ComponentFixture<AnimalsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AnimalsComponent],
      imports: [
        MatDialogModule,
        LoadingIndicatorComponent,
        LayoutDefaultComponent,
        MatIconModule,
        MatProgressSpinnerModule,
      ],
      providers: [{ provide: ActivatedRoute, useValue: {} }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Initialization', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Template', () => {
    it('should have class "animals"', () => {
      const el: HTMLElement = fixture.debugElement.query(
        By.css('app-layout-default')
      ).nativeElement;

      expect(el.classList).toContain('animals');
    });

    it('should have a floating action button with "add" Icon', () => {
      const el: HTMLElement = fixture.debugElement.query(
        By.css('[data-testid="add"] mat-icon')
      ).nativeElement;

      expect(el.textContent).toBe('add');
    });

    it('should indicate a loading spinner', () => {
      const el: HTMLElement = fixture.debugElement.query(
        By.css('app-loading-indicator[data-testid="loading"]')
      ).nativeElement;

      expect(el).toBeTruthy();
    });
  });

  describe('OpenDialog', () => {
    it('should be called once', () => {
      spyOn(component, 'openDialog');

      component.openDialog();

      expect(component.openDialog).toHaveBeenCalled();
    });
  });
});
