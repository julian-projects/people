import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { IPerson } from '@core/feature/people/interfaces/IPerson';
import { PeopleService } from '@feature/people/services/people/people.service';
import { TrackType } from '@shared/services/tracking/track-type';
import { TrackingService } from '@shared/services/tracking/tracking.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PeopleComponent implements OnInit {
  public people$: Observable<IPerson[]>;

  constructor(
    private peopleService: PeopleService,
    private trackingService: TrackingService
  ) {
    this.people$ = this.peopleService.getPeople({ results: 50 });
  }

  ngOnInit(): void {
    this.trackingService.track(TrackType.Pageview, { name: 'People' });
  }
}
