import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IParameterOptions } from '@core/feature/people/interfaces/IParameterOptions';
import { IPerson } from '@core/feature/people/interfaces/IPerson';
import { ServiceBase } from '@shared/services/service.base';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

/**
 * A Service to fetch people from randomuser.me
 */
@Injectable({
  providedIn: 'root',
})
export class PeopleService extends ServiceBase {
  private _apiBaseUrl: string = 'https://randomuser.me/api/';

  constructor(private http: HttpClient) {
    super();
  }

  public get apiBaseUrl(): string {
    return this._apiBaseUrl;
  }

  /**
   *
   * @param apiBaseUrl
   * @constructor
   */
  public Init(apiBaseUrl: string): PeopleService {
    this._apiBaseUrl = apiBaseUrl;

    return this;
  }

  /**
   *
   * @param requestParams
   */
  public getPeople(requestParams?: IParameterOptions): Observable<IPerson[]> {
    let params: string = '';

    if (requestParams) {
      Object.entries(requestParams).forEach(([key, value]) => {
        params += `?${key}=${value}&`;
      });
    }

    const url = `${this._apiBaseUrl}${params}`;

    return this.http.get<any>(url).pipe(
      map(value => value.results as IPerson[]),
      catchError(this.handleError<IPerson[]>('getPeople'))
    );
  }
}
