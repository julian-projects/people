import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { DefaultNavigationComponent } from '@shared/components/default-navigation/default-navigation.component';

describe('DefaultNavigationComponent', () => {
  let component: DefaultNavigationComponent;
  let fixture: ComponentFixture<DefaultNavigationComponent>;

  const fakeActivatedRoute = {
    snapshot: {
      queryParamMap: {
        get(): number {
          return 6;
        },
      },
    },
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DefaultNavigationComponent],
      providers: [{ provide: ActivatedRoute, useValue: fakeActivatedRoute }],
    }).compileComponents();

    fixture = TestBed.createComponent(DefaultNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
