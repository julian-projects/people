import { Injectable } from '@angular/core';
import { TrackType } from '@shared/services/tracking/track-type';
import mixpanel from 'mixpanel-browser';

@Injectable({
  providedIn: 'root',
})
export class TrackingService {
  constructor() {
    mixpanel.init('a889681b170e3617dc5d7ae75dce98f5', { debug: true });
    // mixpanel.identify(userToken);
  }

  /**
   * Push new action to mixpanel.
   *
   * @param {string} id Name of the action to track.
   * @param {*} [action={}] Actions object with custom properties.
   * @memberof MixpanelService
   */
  public track(id: TrackType, action: any = {}): void {
    mixpanel.track(id.toString(), action);
  }

  public optOut(): void {
    mixpanel.opt_out_tracking();
  }

  public optIn(): void {
    mixpanel.opt_in_tracking();
  }
}
