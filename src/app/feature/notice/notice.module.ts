import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { NoticeRoutingModule } from '@feature/notice/notice-routing.module';
import { NoticeComponent } from '@feature/notice/pages/notice/notice.component';
import { LayoutMinimalComponent } from '@shared/components/layouts/layout-minimal/layout-minimal.component';

@NgModule({
  declarations: [NoticeComponent],
  imports: [
    CommonModule,
    NoticeRoutingModule,
    LayoutMinimalComponent,
    MatCardModule,
    MatButtonModule,
  ],
})
export class NoticeModule {}
