import { Observable, throwError } from 'rxjs';

export abstract class ServiceBase {
  protected handleError<T>(
    operation = 'operation',
    useConsole: boolean = false
  ) {
    return (error: any): Observable<T> => {
      if (useConsole) console.error(operation, error);

      return throwError(error as Error);
    };
  }
}
