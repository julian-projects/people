import { Component, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { IAnimal } from '@core/feature/animals/interfaces/IAnimal';
import { IAnimalDialogData } from '@core/feature/animals/interfaces/IAnimalDialogData';
import { AnimalFormComponent } from '@feature/animals/components/animal-form/animal-form.component';

@Component({
  selector: 'app-animal-dialog',
  templateUrl: './animal-dialog.component.html',
  styleUrls: ['./animal-dialog.component.scss'],
})
export class AnimalDialogComponent {
  @ViewChild(AnimalFormComponent)
  public animalForm!: AnimalFormComponent;

  public title!: string;
  public isFormValid: boolean = false;
  public isSubmitting: boolean = false;
  public edit!: IAnimal;

  constructor(
    @Inject(MAT_DIALOG_DATA) data: IAnimalDialogData,
    private dialogRef: MatDialogRef<AnimalDialogComponent>
  ) {
    this.title = data.title;

    if (data.edit) {
      this.edit = data.edit;
    }
  }

  /**
   *
   */
  public save(): void {
    this.animalForm.save();
  }

  /**
   *
   * @param animal
   */
  public onSubmitted(animal: IAnimal): void {
    this.isSubmitting = true;
  }

  /**
   *
   * @param animal
   */
  public onSaved(animal: IAnimal): void {
    this.isSubmitting = false;
    this.close();
  }

  /**
   *
   * @param state
   */
  public onFormStateChanged(state: boolean): void {
    this.isFormValid = state;
  }

  /**
   *
   */
  public close(): void {
    this.dialogRef.close();
  }
}
