import { Gender } from '../enums/gender';

export interface IParameterOptions {
  gender?: Gender;
  results?: number;
}
