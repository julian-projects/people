import { ISpecies } from '@core/feature/animals/interfaces/ISpecies';

export class SpeciesBuilder {
  private _id: string = '';
  private _name: string = '';

  /**
   *
   * @param value
   * @constructor
   */
  public WithId(value: string): SpeciesBuilder {
    this._id = value;

    return this;
  }

  /**
   *
   * @param value
   * @constructor
   */
  public WithName(value: string = 'default'): SpeciesBuilder {
    this._name = value;

    return this;
  }

  /**
   *
   * @constructor
   */
  public Build(): ISpecies {
    return {
      id: this._id,
      name: this._name,
    };
  }
}
