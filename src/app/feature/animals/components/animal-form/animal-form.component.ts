import { CommonModule } from '@angular/common';
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {
  ReactiveFormsModule,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { DateAdapter, MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { IAnimal } from '@core/feature/animals/interfaces/IAnimal';
import { IFood } from '@core/feature/animals/interfaces/IFood';
import { ISpecies } from '@core/feature/animals/interfaces/ISpecies';
import { AnimalServiceProxy } from '@feature/animals/services/animal/animal.service.proxy';
import { FoodService } from '@feature/animals/services/food/food.service';
import { SpeciesService } from '@feature/animals/services/species/species.service';
import { LoadingIndicatorComponent } from '@shared/components/loading-indicator/loading-indicator.component';
import { combineLatest, Observable, Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  first,
  map,
  startWith,
  takeUntil,
  tap,
} from 'rxjs/operators';

@Component({
  selector: 'app-animal-form',
  standalone: true,
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    LoadingIndicatorComponent,
    MatDatepickerModule,
    MatNativeDateModule,
  ],
  templateUrl: './animal-form.component.html',
  styleUrls: ['./animal-form.component.scss'],
})
export class AnimalFormComponent implements OnInit, OnDestroy {
  @Input()
  public edit!: IAnimal;

  /**
   * Gets emitted at the start of saving a new entity
   */
  @Output()
  public submitted: EventEmitter<IAnimal> = new EventEmitter<IAnimal>();
  /**
   * Gets emitted at the end of saving a new entity
   */
  @Output()
  public saved: EventEmitter<IAnimal> = new EventEmitter<IAnimal>();
  /**
   * Get emitted when form validation state changes
   */
  @Output()
  public formStateChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild('speciesSearch')
  private speciesSearch!: ElementRef<HTMLInputElement>;

  public form!: UntypedFormGroup;
  public data$!: Observable<[ISpecies[], IFood[]]>;
  public filteredSpecies$!: Observable<ISpecies[]>;
  public maxDate!: Date;
  public minDate!: Date;

  private readonly _food$!: Observable<IFood[]>;
  private readonly _species$!: Observable<ISpecies[]>;

  private _isSubmitting: boolean = false;
  private readonly _destroySubject = new Subject<void>();
  private readonly _locale = 'en-GB';

  constructor(
    private fb: UntypedFormBuilder,
    private animalService: AnimalServiceProxy,
    private foodService: FoodService,
    private speciesService: SpeciesService,
    private dateAdapter: DateAdapter<Date>
  ) {
    this._species$ = this.speciesService.getAllSpecies().pipe(
      tap(list => {
        this.filteredSpecies$ = this.form.controls[
          'speciesFilter'
        ].valueChanges.pipe(
          debounceTime(250),
          startWith(''),
          map(value => {
            return this._filter(value || '', list);
          })
        );
      })
    );

    this._food$ = this.foodService.getAllFood();

    this.data$ = combineLatest([this._species$, this._food$]);

    this.dateAdapter.setLocale(this._locale);
    let currentYear = new Date().getFullYear();
    this.maxDate = new Date(currentYear, 11, 31);
    this.minDate = new Date(currentYear - 100, 11, 31);
  }

  public get isSubmitting(): boolean {
    return this._isSubmitting;
  }

  public set isSubmitting(value: boolean) {
    if (value) {
      this.form.disable();
    } else {
      this.form.enable();
    }

    this._isSubmitting = value;
  }

  ngOnInit(): void {
    this._initFormValues();

    this.form.statusChanges
      .pipe(distinctUntilChanged(), takeUntil(this._destroySubject))
      .subscribe(status => {
        this.formStateChanged.emit(this.form.valid);
      });
  }

  ngOnDestroy(): void {
    this._destroySubject.next();
    this._destroySubject.complete();
  }

  /**
   *
   */
  public save(): void {
    if (!this.form.valid) return;

    this.isSubmitting = true;
    const animal: IAnimal = this._getAnimalFormData();
    this.submitted.emit(animal);

    if (this.edit) {
      this._updateAnimal(animal);
    } else {
      this._addAnimal(animal);
    }
  }

  /**
   *
   */
  public onSpeciesSelectOpened() {
    this.speciesSearch.nativeElement.focus();
  }

  /**
   *
   * @param index
   * @param value
   */
  public speciesTrackBy(index: number, value: ISpecies): string {
    return value.id;
  }

  /**
   *
   * @param index
   * @param value
   */
  public favoriteFoodTrackBy(index: number, value: IFood): string {
    return value.id;
  }

  /**
   *
   * @private
   */
  private _initFormValues(): void {
    const values = {
      name: this.edit ? this.edit.name : '',
      species: this.edit ? this.edit.species : '',
      birthday: this.edit ? this.edit.birthday : '',
      description: this.edit ? this.edit.description : '',
      favoriteFood: this.edit ? this.edit.favoriteFood : '',
    };

    this.form = this.fb.group({
      name: [values.name, Validators.required],
      species: [values.species, Validators.required],
      speciesFilter: [''],
      birthday: [new Date(values.birthday)],
      description: [values.description],
      favoriteFood: [values.favoriteFood],
    });
  }

  /**
   *
   * @param animal
   * @private
   */
  private _updateAnimal(animal: IAnimal): void {
    this.animalService
      .updateAnimal(this.edit.id, animal)
      .pipe(takeUntil(this._destroySubject))
      .subscribe((animal: IAnimal) => {
        this.isSubmitting = false;
        this.saved.emit(animal);
      });
  }

  /**
   *
   * @param animal
   * @private
   */
  private _addAnimal(animal: IAnimal): void {
    this.animalService
      .addAnimal(animal)
      .pipe(first(), takeUntil(this._destroySubject))
      .subscribe((animal: IAnimal) => {
        this.isSubmitting = false;
        this.saved.emit(animal);
      });
  }

  /**
   *
   * @param value
   * @param list
   * @private
   */
  private _filter(value: string, list: ISpecies[] | IFood[]): ISpecies[] {
    const filterValue = value.toLowerCase();

    return list.filter(option => {
      return option.name.toLowerCase().includes(filterValue);
    });
  }

  /**
   *
   * @private
   */
  private _getAnimalFormData(): IAnimal {
    const controls = this.form.controls;

    return {
      id: '0',
      name: controls['name'].value,
      species: controls['species'].value,
      birthday: controls['birthday'].value.getTime(),
      description: controls['description'].value,
      favoriteFood: controls['favoriteFood'].value,
    };
  }
}
