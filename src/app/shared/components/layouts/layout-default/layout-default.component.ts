import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { DefaultNavigationComponent } from '@shared/components/default-navigation/default-navigation.component';

@Component({
  selector: 'app-layout-default',
  templateUrl: './layout-default.component.html',
  styleUrls: ['./layout-default.component.scss'],
  standalone: true,
  imports: [CommonModule, DefaultNavigationComponent],
})
export class LayoutDefaultComponent {}
