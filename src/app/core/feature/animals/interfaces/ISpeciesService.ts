import { Observable } from 'rxjs';

import { ISpecies } from './ISpecies';

export interface ISpeciesService {
  /**
   *
   */
  getAllSpecies(): Observable<ISpecies[]>;

  /**
   *
   * @param id
   */
  getSpecies(id: string): Observable<ISpecies | undefined>;

  /**
   *
   * @param species
   */
  addSpecies(species: ISpecies): Observable<ISpecies>;

  /**
   *
   * @param id
   */
  deleteSpecies(id: string): Observable<ISpecies>;

  /**
   *
   * @param id
   * @param updateData
   */
  updateSpecies(
    id: string,
    updateData: Partial<ISpecies>
  ): Observable<ISpecies>;
}
