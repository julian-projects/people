import { IAnimal } from '@core/feature/animals/interfaces/IAnimal';

export interface IAnimalDialogData {
  id: string;
  title: string;
  edit: IAnimal;
}
