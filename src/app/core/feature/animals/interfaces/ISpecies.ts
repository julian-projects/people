export interface ISpecies {
  id: string;
  name: string;
}
