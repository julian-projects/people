import { Observable } from 'rxjs';

import { IAnimal } from './IAnimal';

export interface IAnimalService {
  /**
   * Returns an Observable List of animals
   */
  getAnimals(): Observable<IAnimal[]>;

  /**
   * Returns a specific animal observable based on the given id
   * @param id
   */
  getAnimal(id: string): Observable<IAnimal>;

  /**
   * Adds a new animal and
   * returns the added animal observable
   * @param animal
   */
  addAnimal(animal: IAnimal): Observable<IAnimal>;

  /**
   * Deletes a specific animal based on the given id and
   * returns the deleted animal observable
   * @param id
   */
  deleteAnimal(id: string): Observable<IAnimal>;

  /**
   * Updates a specific animal based on the given id and
   * returns the updated animal observable
   * @param id
   * @param updateData
   */
  updateAnimal(id: string, updateData: Partial<IAnimal>): Observable<IAnimal>;
}
