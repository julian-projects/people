import { IFood } from '@core/feature/animals/interfaces/IFood';

export class FoodBuilder {
  private _id: string = '';
  private _name: string = '';
  private _description: string = '';

  /**
   *
   * @param value
   * @constructor
   */
  public WithId(value: string): FoodBuilder {
    this._id = value;

    return this;
  }

  /**
   *
   * @param value
   * @constructor
   */
  public WithName(value: string = 'default'): FoodBuilder {
    this._name = value;

    return this;
  }

  /**
   *
   * @param value
   * @constructor
   */
  public WithDescription(value: string = 'default'): FoodBuilder {
    this._description = value;

    return this;
  }

  /**
   *
   * @constructor
   */
  public Build(): IFood {
    return {
      id: this._id,
      name: this._name,
      description: this._description,
    };
  }
}
