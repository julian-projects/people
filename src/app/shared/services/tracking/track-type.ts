export enum TrackType {
  Pageview = 'Pageview',
  Click = 'Click',
}
