import { IAnimal } from '@core/feature/animals/interfaces/IAnimal';
import { AnimalService } from '@feature/animals/services/animal/animal.service';

export class AnimalServiceBuilder {
  private _animals: IAnimal[] = [];
  private _fakeDelay: number = 0;

  /**
   *
   * @param animals
   * @constructor
   */
  public WithAnimals(animals: IAnimal[]): AnimalServiceBuilder {
    this._animals = animals;

    return this;
  }

  /**
   *
   * @param value
   * @constructor
   */
  public WithFakeDelay(value: number): AnimalServiceBuilder {
    this._fakeDelay = value;

    return this;
  }

  /**
   *
   * @constructor
   */
  public Build(): AnimalService {
    return new AnimalService().Init(this._animals, this._fakeDelay);
  }
}
