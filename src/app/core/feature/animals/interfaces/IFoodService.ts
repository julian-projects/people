import { Observable } from 'rxjs';

import { IFood } from './IFood';

export interface IFoodService {
  /**
   *
   */
  getAllFood(): Observable<IFood[]>;

  /**
   *
   * @param id
   */
  getFood(id: string): Observable<IFood | undefined>;

  /**
   *
   * @param food
   */
  addFood(food: IFood): Observable<IFood>;

  /**
   *
   * @param id
   */
  deleteFood(id: string): Observable<IFood>;

  /**
   *
   * @param id
   * @param updateData
   */
  updateFood(id: string, updateData: Partial<IFood>): Observable<IFood>;
}
