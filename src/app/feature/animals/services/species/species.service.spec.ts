import { TestBed } from '@angular/core/testing';
import { ISpecies } from '@core/feature/animals/interfaces/ISpecies';
import { SpeciesBuilder } from '@feature/animals/services/species/species.builder';
import { SpeciesService } from '@feature/animals/services/species/species.service';
import { SpeciesServiceBuilder } from '@feature/animals/services/species/species.service.builder';
import { take } from 'rxjs/operators';

describe('SpeciesService', () => {
  const notAvailableIndex = '99';
  let service: SpeciesService;

  beforeEach(() => {
    const species1 = new SpeciesBuilder()
      .WithId('1')
      .WithName('Banana')
      .Build();
    const species2 = new SpeciesBuilder().WithId('2').WithName('Apple').Build();

    const serviceStub = new SpeciesServiceBuilder()
      .WithSpecies([species1, species2])
      .Build();

    TestBed.configureTestingModule({
      providers: [
        {
          provide: SpeciesService,
          useValue: serviceStub,
        },
      ],
    });

    service = TestBed.inject(SpeciesService);
  });

  describe('Initialization', () => {
    it('should be created', () => {
      expect(service).toBeTruthy();
    });

    it('should be initialized with zero species', async () => {
      const serviceStub = new SpeciesServiceBuilder().Build();

      TestBed.resetTestingModule();
      TestBed.overrideProvider(SpeciesService, {
        useValue: serviceStub,
      });

      service = TestBed.inject(SpeciesService);

      const expected = await service.getAllSpecies().pipe(take(1)).toPromise();

      expect(expected.length).toBe(0);
    });

    it('should be initialized with two species', async () => {
      const expected = await service.getAllSpecies().pipe(take(1)).toPromise();

      expect(expected.length).toBe(2);
    });
  });

  describe('Add', () => {
    it('should add an species named "New"', async () => {
      const data = new SpeciesBuilder().WithName('New').Build();
      const expected = await service.addSpecies(data).pipe(take(1)).toPromise();

      expect(expected.name).toBe('New');
    });
  });

  describe('Get', () => {
    it('should return one species by id "1"', async () => {
      const expected = await service.getSpecies('1').pipe(take(1)).toPromise();

      expect(expected?.id).toBe('1');
    });

    it('should throw an error of type "Error" for item not found', async () => {
      try {
        await service.getSpecies(notAvailableIndex).pipe(take(1)).toPromise();
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
      }
    });
  });

  describe('Delete', () => {
    it('should delete species with id "1"', async () => {
      const expected = await service
        .deleteSpecies('1')
        .pipe(take(1))
        .toPromise();

      expect(expected.id).toBe('1');
    });

    it('should throw an error for item not found', async () => {
      try {
        await service
          .deleteSpecies(notAvailableIndex)
          .pipe(take(1))
          .toPromise();
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
      }
    });

    it('should receive deleted item', async () => {
      const expected = await service.getSpecies('1').pipe(take(1)).toPromise();
      const result = await service.deleteSpecies('1').pipe(take(1)).toPromise();

      expect(result).toBe(expected as ISpecies);
    });
  });

  describe('Update', () => {
    it('should update name of species with id "1" to "Updated"', async () => {
      const updateData: Partial<ISpecies> = {
        name: 'Updated',
      };

      const expected = await service
        .updateSpecies('1', updateData)
        .pipe(take(1))
        .toPromise();

      expect(expected.name).toBe('Updated');
    });

    it('should throw an error for item not found', async () => {
      const updateData: Partial<ISpecies> = {
        name: 'Updated',
      };

      try {
        await service
          .updateSpecies(notAvailableIndex, updateData)
          .pipe(take(1))
          .toPromise();

        fail('Item should not exists');
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
      }
    });
  });
});
