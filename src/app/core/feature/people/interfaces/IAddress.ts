import { IStreet } from './IStreet';

export interface IAddress {
  street: IStreet;
  city: string;
  state: string;
  country: string;
  postcode: string;
}
