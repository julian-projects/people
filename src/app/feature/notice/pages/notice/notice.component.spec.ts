import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { NoticeComponent } from '@feature/notice/pages/notice/notice.component';
import { LayoutMinimalComponent } from '@shared/components/layouts/layout-minimal/layout-minimal.component';

describe('NoticeComponent', () => {
  let component: NoticeComponent;
  let fixture: ComponentFixture<NoticeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NoticeComponent],
      imports: [MatCardModule, LayoutMinimalComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(NoticeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
