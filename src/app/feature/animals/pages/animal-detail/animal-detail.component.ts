import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IAnimal } from '@core/feature/animals/interfaces/IAnimal';
import { AnimalServiceProxy } from '@feature/animals/services/animal/animal.service.proxy';
import { TrackType } from '@shared/services/tracking/track-type';
import { TrackingService } from '@shared/services/tracking/tracking.service';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-animal-detail',
  templateUrl: './animal-detail.component.html',
  styleUrls: ['./animal-detail.component.scss'],
})
export class AnimalDetailComponent implements OnInit, OnDestroy {
  public animal$!: Observable<IAnimal | undefined>;

  private readonly _destroySubject = new Subject<void>();

  constructor(
    private animalService: AnimalServiceProxy,
    private route: ActivatedRoute,
    private trackingService: TrackingService
  ) {
    route.paramMap.pipe(takeUntil(this._destroySubject)).subscribe(params => {
      const id = params.get('id');

      if (id === null) return;

      this.animal$ = animalService.getAnimal(id);
    });
  }

  ngOnInit(): void {
    this.trackingService.track(TrackType.Pageview, { name: 'Animal-Detail' });
  }

  ngOnDestroy(): void {
    this._destroySubject.next();
    this._destroySubject.complete();
  }
}
