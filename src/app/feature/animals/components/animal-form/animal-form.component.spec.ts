import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimalFormComponent } from './animal-form.component';

describe('AnimalDialogContentComponent', () => {
  let component: AnimalFormComponent;
  let fixture: ComponentFixture<AnimalFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AnimalFormComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(AnimalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Initialization', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should be initialized with "isSubmitting" false', () => {
      expect(component.isSubmitting).toBe(false);
    });
  });

  describe('Component', () => {
    it('should disable form by setting "isSubmitting" to true', () => {
      component.isSubmitting = true;

      expect(component.form.disabled).toBe(true);
    });

    it('should enable form by setting "isSubmitting" to false', () => {
      component.isSubmitting = false;

      expect(component.form.disabled).toBe(false);
    });
  });
});
