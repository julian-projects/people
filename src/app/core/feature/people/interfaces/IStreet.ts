export interface IStreet {
  name: string;
  number: string;
}
