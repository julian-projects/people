import { Injectable } from '@angular/core';
import { IAnimal } from '@core/feature/animals/interfaces/IAnimal';
import { IAnimalService } from '@core/feature/animals/interfaces/IAnimalService';
import { AnimalServiceProxy } from '@feature/animals/services/animal/animal.service.proxy';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AnimalServiceFacade implements IAnimalService {
  constructor(private animalService: AnimalServiceProxy) {}

  addAnimal(animal: IAnimal): Observable<IAnimal> {
    return this.animalService.addAnimal(animal);
  }

  deleteAnimal(id: string): Observable<IAnimal> {
    return this.animalService.deleteAnimal(id);
  }

  getAnimal(id: string): Observable<IAnimal> {
    return this.animalService.getAnimal(id);
  }

  getAnimals(): Observable<IAnimal[]> {
    return this.animalService.getAnimals();
  }

  updateAnimal(id: string, updateData: Partial<IAnimal>): Observable<IAnimal> {
    return this.animalService.updateAnimal(id, updateData);
  }
}
