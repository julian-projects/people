import { Gender } from '../enums/gender';
import { IAddress } from './IAddress';

export interface IPerson {
  name: {
    title: string;
    first: string;
    last: string;
  };
  age: number;
  gender: Gender;
  address: IAddress;
  picture: {
    large: string;
    medium: string;
    thumbnail: string;
  };
}
