import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { TrackType } from '@shared/services/tracking/track-type';
import { TrackingService } from '@shared/services/tracking/tracking.service';

@Component({
  selector: 'app-notice',
  templateUrl: './notice.component.html',
  styleUrls: ['./notice.component.scss'],
})
export class NoticeComponent implements OnInit {
  constructor(
    private trackingService: TrackingService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.trackingService.track(TrackType.Pageview, { name: 'Notice' });
  }

  public navigateBack(): void {
    this.trackingService.track(TrackType.Click, { name: 'Navigate Back' });
    this.location.back();
  }
}
