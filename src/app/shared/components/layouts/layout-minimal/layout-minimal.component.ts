import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-layout-minimal',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './layout-minimal.component.html',
  styleUrls: ['./layout-minimal.component.scss'],
})
export class LayoutMinimalComponent {}
