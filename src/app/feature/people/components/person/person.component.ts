import { Component, Input } from '@angular/core';
import { IPerson } from '@core/feature/people/interfaces/IPerson';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss'],
})
export class PersonComponent {
  @Input() public person!: IPerson;

  get fullname(): string {
    return this.person.name.first + ' ' + this.person.name.last;
  }
}
