import { TestBed } from '@angular/core/testing';
import { IAnimal } from '@core/feature/animals/interfaces/IAnimal';
import { AnimalBuilder } from '@feature/animals/services/animal/animal.builder';
import { AnimalService } from '@feature/animals/services/animal/animal.service';
import { AnimalServiceBuilder } from '@feature/animals/services/animal/animal.service.builder';
import { take } from 'rxjs/operators';

describe('AnimalService', () => {
  const notAvailableIndex = '99';
  let service: AnimalService;

  beforeEach(() => {
    const animal = new AnimalBuilder().WithId('1').WithName().Build();
    const animal2 = new AnimalBuilder().WithId('2').WithName().Build();

    const serviceStub = new AnimalServiceBuilder()
      .WithAnimals([animal, animal2])
      .WithFakeDelay(0)
      .Build();

    TestBed.configureTestingModule({
      providers: [
        {
          provide: AnimalService,
          useValue: serviceStub,
        },
      ],
    });

    service = TestBed.inject(AnimalService);
  });

  describe('Initialization', () => {
    it('should be created', () => {
      expect(service).toBeTruthy();
    });

    it('should be initialized with zero animals', async () => {
      const serviceStub = new AnimalServiceBuilder().Build();

      TestBed.resetTestingModule();
      TestBed.overrideProvider(AnimalService, {
        useValue: serviceStub,
      });

      service = TestBed.inject(AnimalService);

      const expected = await service.getAnimals().pipe(take(1)).toPromise();

      expect(expected.length).toBe(0);
    });

    it('should be initialized with two animal', async () => {
      const expected = await service.getAnimals().pipe(take(1)).toPromise();

      expect(expected.length).toBe(2);
    });
  });

  describe('Add', () => {
    it('should add an animal named "New"', async () => {
      const data = new AnimalBuilder().WithName('New').Build();
      const expected = await service.addAnimal(data).pipe(take(1)).toPromise();

      expect(expected.name).toBe('New');
    });
  });

  describe('Get', () => {
    it('should return one animal by id "1"', async () => {
      const expected = await service.getAnimal('1').pipe(take(1)).toPromise();

      expect(expected?.id).toBe('1');
    });

    it('should throw an error of type "Error" for item not found', async () => {
      try {
        await service.getAnimal(notAvailableIndex).pipe(take(1)).toPromise();
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
      }
    });
  });

  describe('Delete', () => {
    it('should delete animal with id "1"', async () => {
      const expected = await service
        .deleteAnimal('1')
        .pipe(take(1))
        .toPromise();

      expect(expected.id).toBe('1');
    });

    it('should throw an error for item not found', async () => {
      try {
        await service.deleteAnimal(notAvailableIndex).pipe(take(1)).toPromise();
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
      }
    });

    it('should receive deleted item', async () => {
      const expected = await service.getAnimal('1').pipe(take(1)).toPromise();
      const result = await service.deleteAnimal('1').pipe(take(1)).toPromise();

      expect(result).toBe(expected as IAnimal);
    });
  });

  describe('Update', () => {
    it('should update name of animal with id "1" to "Updated"', async () => {
      const updateData: Partial<IAnimal> = {
        name: 'Updated',
      };

      const expected = await service
        .updateAnimal('1', updateData)
        .pipe(take(1))
        .toPromise();

      expect(expected.name).toBe('Updated');
    });

    it('should throw an error for item not found', async () => {
      const updateData: Partial<IAnimal> = {
        name: 'Updated',
      };

      try {
        await service
          .updateAnimal(notAvailableIndex, updateData)
          .pipe(take(1))
          .toPromise();
        fail('Item should not exists');
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
      }
    });
  });
});
