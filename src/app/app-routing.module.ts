import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const titlePrefix = 'Project -';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'people',
    pathMatch: 'full',
  },
  {
    path: 'people',
    title: `${titlePrefix} People`,
    loadChildren: () =>
      import('./feature/people/people.module').then(m => m.PeopleModule),
  },
  {
    path: 'animals',
    title: `${titlePrefix} Animals`,
    loadChildren: () =>
      import('./feature/animals/animals.module').then(m => m.AnimalsModule),
  },
  {
    path: 'notice',
    title: `${titlePrefix} Notice`,
    loadChildren: () =>
      import('./feature/notice/notice.module').then(m => m.NoticeModule),
  },
  {
    path: '**',
    redirectTo: 'notice',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
