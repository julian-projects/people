import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { IAnimal } from '@core/feature/animals/interfaces/IAnimal';
import { AnimalComponent } from '@feature/animals/components/animal/animal.component';
import { AnimalBuilder } from '@feature/animals/services/animal/animal.builder';

describe('AnimalComponent', () => {
  let component: AnimalComponent;
  let fixture: ComponentFixture<AnimalComponent>;

  const fakeActivatedRoute = {
    snapshot: {
      queryParamMap: {
        get(): number {
          return 6;
        },
      },
    },
  };

  const animalInput: IAnimal = new AnimalBuilder()
    .WithName()
    .WithId('1')
    .Build();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AnimalComponent],
      imports: [RouterModule, MatDialogModule, MatCardModule],
      providers: [
        { provide: ActivatedRoute, useValue: fakeActivatedRoute },
        MatSnackBar,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimalComponent);
    component = fixture.componentInstance;
    component.animal = animalInput;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
