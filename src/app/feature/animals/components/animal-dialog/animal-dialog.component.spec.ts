import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormBuilder } from '@angular/forms';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AnimalDialogComponent } from '@feature/animals/components/animal-dialog/animal-dialog.component';
import { AnimalFormComponent } from '@feature/animals/components/animal-form/animal-form.component';

describe('DialogAnimalComponent', () => {
  let component: AnimalDialogComponent;
  let fixture: ComponentFixture<AnimalDialogComponent>;

  const dialogData = {
    title: '',
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatProgressSpinnerModule, AnimalFormComponent, MatDialogModule],
      declarations: [AnimalDialogComponent],
      providers: [
        UntypedFormBuilder,
        {
          provide: MatDialogRef,
          useFactory: () =>
            jasmine.createSpyObj('MatDialogRef', ['close', 'afterClosed']),
        },
        { provide: MAT_DIALOG_DATA, useValue: dialogData },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimalDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Initialization', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should be initialized with "isSubmitting" false', () => {
      expect(component.isSubmitting).toBe(false);
    });
  });
});
