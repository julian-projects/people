import { TestBed } from '@angular/core/testing';
import { IFood } from '@core/feature/animals/interfaces/IFood';
import { FoodBuilder } from '@feature/animals/services/food/food.builder';
import { FoodService } from '@feature/animals/services/food/food.service';
import { FoodServiceBuilder } from '@feature/animals/services/food/food.service.builder';
import { take } from 'rxjs/operators';

describe('FoodService', () => {
  const notAvailableIndex = '99';
  let service: FoodService;

  beforeEach(() => {
    const food1 = new FoodBuilder().WithId('1').WithName('Banana').Build();
    const food2 = new FoodBuilder().WithId('2').WithName('Apple').Build();

    const serviceStub = new FoodServiceBuilder()
      .WithFood([food1, food2])
      .Build();

    TestBed.configureTestingModule({
      providers: [
        {
          provide: FoodService,
          useValue: serviceStub,
        },
      ],
    });

    service = TestBed.inject(FoodService);
  });

  describe('Initialization', () => {
    it('should be created', () => {
      expect(service).toBeTruthy();
    });

    it('should be initialized with zero food', async () => {
      const serviceStub = new FoodServiceBuilder().Build();

      TestBed.resetTestingModule();
      TestBed.overrideProvider(FoodService, {
        useValue: serviceStub,
      });

      service = TestBed.inject(FoodService);

      const expected = await service.getAllFood().pipe(take(1)).toPromise();

      expect(expected.length).toBe(0);
    });

    it('should be initialized with two food', async () => {
      const expected = await service.getAllFood().pipe(take(1)).toPromise();

      expect(expected.length).toBe(2);
    });
  });

  describe('Add', () => {
    it('should add an food named "New"', async () => {
      const data = new FoodBuilder().WithName('New').Build();
      const expected = await service.addFood(data).pipe(take(1)).toPromise();

      expect(expected.name).toBe('New');
    });
  });

  describe('Get', () => {
    it('should return one food by id "1"', async () => {
      const expected = await service.getFood('1').pipe(take(1)).toPromise();

      expect(expected?.id).toBe('1');
    });

    it('should throw an error of type "Error" for item not found', async () => {
      try {
        await service.getFood(notAvailableIndex).pipe(take(1)).toPromise();
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
      }
    });
  });

  describe('Delete', () => {
    it('should delete food with id "1"', async () => {
      const expected = await service.deleteFood('1').pipe(take(1)).toPromise();

      expect(expected.id).toBe('1');
    });

    it('should throw an error for item not found', async () => {
      try {
        await service.deleteFood(notAvailableIndex).pipe(take(1)).toPromise();
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
      }
    });

    it('should receive deleted item', async () => {
      const expected = await service.getFood('1').pipe(take(1)).toPromise();
      const result = await service.deleteFood('1').pipe(take(1)).toPromise();

      expect(result).toBe(expected as IFood);
    });
  });

  describe('Update', () => {
    it('should update name of food with id "1" to "Updated"', async () => {
      const updateData: Partial<IFood> = {
        name: 'Updated',
      };

      const expected = await service
        .updateFood('1', updateData)
        .pipe(take(1))
        .toPromise();

      expect(expected.name).toBe('Updated');
    });

    it('should throw an error for item not found', async () => {
      const updateData: Partial<IFood> = {
        name: 'Updated',
      };

      try {
        await service
          .updateFood(notAvailableIndex, updateData)
          .pipe(take(1))
          .toPromise();
        fail('Item should not exists');
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
      }
    });
  });
});
