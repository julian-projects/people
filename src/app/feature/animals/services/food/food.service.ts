import { Injectable } from '@angular/core';
import { IFood } from '@core/feature/animals/interfaces/IFood';
import { IFoodService } from '@core/feature/animals/interfaces/IFoodService';
import { ServiceBase } from '@shared/services/service.base';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, delay, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class FoodService extends ServiceBase implements IFoodService {
  private _food$: BehaviorSubject<IFood[]> = new BehaviorSubject([
    {
      id: 'fwadwaw',
      name: 'Banana',
      description: 'Cool Banana',
    },
    {
      id: 'hwuahiu213',
      name: 'Apple',
      description: 'Cool Apple',
    },
  ]);
  private _fakeDelay: number = 350;

  constructor() {
    super();
  }

  /**
   *
   * @param food
   * @param fakeDelay
   * @constructor
   */
  public Init(food: IFood[], fakeDelay: number): FoodService {
    this._food$.next(food);
    this._fakeDelay = fakeDelay;

    return this;
  }

  getAllFood(): Observable<IFood[]> {
    return this._food$.pipe(
      delay(this._fakeDelay),
      tap(result => {
        result.sort((a, b) => {
          return a.name.localeCompare(b.name);
        });
      }),
      catchError(this.handleError<IFood[]>('getAllFood'))
    );
  }

  getFood(id: string): Observable<IFood | undefined> {
    const food = this._food$.value.find(el => el.id === id);

    return of(food).pipe(
      delay(this._fakeDelay),
      tap(value => {
        if (!value) throw new Error('Item not found!');
      }),
      catchError(this.handleError<IFood>('getFood'))
    );
  }

  addFood(food: IFood): Observable<IFood> {
    return of(food).pipe(
      delay(this._fakeDelay),
      tap(value => {
        this._food$.next([...this._food$.value, food]);
      }),
      catchError(this.handleError<IFood>('addFood'))
    );
  }

  deleteFood(id: string): Observable<IFood> {
    const index = this._food$.value.findIndex(el => el.id === id);
    const food = this._food$.value[index];

    this._food$.value.splice(index, 1);

    return of(food).pipe(
      delay(this._fakeDelay),
      tap(value => {
        if (!value) throw new Error('Item not found!');
      }),
      catchError(this.handleError<IFood>('getFood'))
    );
  }

  updateFood(id: string, updateData: Partial<IFood>): Observable<IFood> {
    const index = this._food$.value.findIndex(el => el.id === id);
    const old = this._food$.value[index];

    return of(old).pipe(
      delay(this._fakeDelay),
      map(value => {
        if (!value) throw new Error('Item not found!');

        const updated = { ...old, ...updateData };
        this._food$.value[index] = updated;
        return updated;
      }),
      catchError(this.handleError<IFood>('getFood'))
    );
  }
}
