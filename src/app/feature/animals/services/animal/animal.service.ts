import { Injectable } from '@angular/core';
import { IAnimal } from '@core/feature/animals/interfaces/IAnimal';
import { IAnimalService } from '@core/feature/animals/interfaces/IAnimalService';
import { ServiceBase } from '@shared/services/service.base';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, delay, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AnimalService extends ServiceBase implements IAnimalService {
  private _animals$: BehaviorSubject<IAnimal[]> = new BehaviorSubject<
    IAnimal[]
  >([]);
  private _fakeDelay: number = 350;

  /**
   * Initializes the services
   * @param animals
   * @param fakeDelay
   * @constructor
   */
  public Init(animals: IAnimal[], fakeDelay: number): AnimalService {
    this._animals$.next(animals);
    this._fakeDelay = fakeDelay;

    return this;
  }

  public getAnimals(): Observable<IAnimal[]> {
    return this._animals$.pipe(
      delay(this._fakeDelay),
      catchError(this.handleError<IAnimal[]>('getAnimals'))
    );
  }

  public getAnimal(id: string): Observable<IAnimal> {
    const animal = this._animals$.value.find(el => el.id === id);

    if (!animal) throw new Error(`Item with id ${id} not found!`);

    return of(animal).pipe(
      delay(this._fakeDelay),
      catchError(this.handleError<IAnimal>('getAnimal'))
    );
  }

  public addAnimal(animal: IAnimal): Observable<IAnimal> {
    animal.id = Date.now().toString();
    animal.createdAt = Date.now();

    return of(animal).pipe(
      delay(this._fakeDelay),
      tap(value => {
        this._animals$.next([...this._animals$.value, animal]);
      }),
      catchError(this.handleError<IAnimal>('addAnimal'))
    );
  }

  public deleteAnimal(id: string): Observable<IAnimal> {
    return of({ id: id } as Pick<IAnimal, 'id'>).pipe(
      delay(this._fakeDelay),
      map(animal => {
        return this.deleteById(animal.id);
      }),
      catchError(this.handleError<IAnimal>('getAnimal', true))
    );
  }

  public updateAnimal(
    id: string,
    updateData: Partial<IAnimal>
  ): Observable<IAnimal> {
    const index = this._animals$.value.findIndex(el => el.id === id);
    const old = this._animals$.value[index];

    updateData.updatedAt = Date.now();

    return of(old).pipe(
      delay(this._fakeDelay),
      map(value => {
        if (!value) throw new Error(`Item with id ${id} not found!`);

        const updated = { ...old, ...updateData };
        this._animals$.value[index] = updated;

        return updated;
      }),
      catchError(this.handleError<IAnimal>('getAnimal'))
    );
  }

  private deleteById(id: string) {
    const index = this._animals$.value.findIndex(el => el.id === id);

    if (index === -1) {
      throw new Error(`Item with id ${id} not found!`);
    }

    return this._animals$.value.splice(index, 1)[0];
  }
}
