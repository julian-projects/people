import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { PersonComponent } from '@feature/people/components/person/person.component';
import { PeopleComponent } from '@feature/people/pages/people/people.component';
import { PeopleRoutingModule } from '@feature/people/people-routing.module';
import { LayoutDefaultComponent } from '@shared/components/layouts/layout-default/layout-default.component';
import { ListingComponent } from '@shared/components/listing/listing.component';
import { LoadingIndicatorComponent } from '@shared/components/loading-indicator/loading-indicator.component';
import { TruncatePipe } from '@shared/pipes/truncate.pipe';

@NgModule({
  declarations: [PeopleComponent, PersonComponent],
  imports: [
    CommonModule,
    PeopleRoutingModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatCardModule,
    LayoutDefaultComponent,
    TruncatePipe,
    ListingComponent,
    LoadingIndicatorComponent,
  ],
})
export class PeopleModule {}
