import { Injectable } from '@angular/core';
import { IAnimal } from '@core/feature/animals/interfaces/IAnimal';
import { IAnimalService } from '@core/feature/animals/interfaces/IAnimalService';
import { AnimalService } from '@feature/animals/services/animal/animal.service';
import { TrackType } from '@shared/services/tracking/track-type';
import { TrackingService } from '@shared/services/tracking/tracking.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AnimalServiceProxy implements IAnimalService {
  constructor(
    private readonly animalService: AnimalService,
    private readonly trackingService: TrackingService
  ) {}

  addAnimal(animal: IAnimal): Observable<IAnimal> {
    this.trackingService.track(TrackType.Click, { name: 'AddAnimal' });

    return this.animalService.addAnimal(animal);
  }

  deleteAnimal(id: string): Observable<IAnimal> {
    this.trackingService.track(TrackType.Click, { name: 'DeleteAnimal' });

    return this.animalService.deleteAnimal(id);
  }

  getAnimal(id: string): Observable<IAnimal> {
    return this.animalService.getAnimal(id);
  }

  getAnimals(): Observable<IAnimal[]> {
    return this.animalService.getAnimals();
  }

  updateAnimal(id: string, updateData: Partial<IAnimal>): Observable<IAnimal> {
    this.trackingService.track(TrackType.Click, { name: 'UpdateAnimal' });

    return this.animalService.updateAnimal(id, updateData);
  }
}
