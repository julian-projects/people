import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
} from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IAnimal } from '@core/feature/animals/interfaces/IAnimal';
import { IAnimalDialogData } from '@core/feature/animals/interfaces/IAnimalDialogData';
import { AnimalDialogComponent } from '@feature/animals/components/animal-dialog/animal-dialog.component';
import { AnimalServiceProxy } from '@feature/animals/services/animal/animal.service.proxy';
import { Subject } from 'rxjs';
import { catchError, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-animal',
  templateUrl: './animal.component.html',
  styleUrls: ['./animal.component.scss'],
  // https://www.youtube.com/watch?v=-tB-QDrPmuI
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AnimalComponent implements OnDestroy {
  @Input() public animal!: IAnimal;

  public isDeleting: boolean = false;

  private readonly _destroySubject = new Subject<void>();

  constructor(
    private readonly dialog: MatDialog,
    private readonly animalService: AnimalServiceProxy,
    private readonly snackbar: MatSnackBar
  ) {}

  get isPerformingAction(): boolean {
    return this.isDeleting;
  }

  ngOnDestroy(): void {
    this._destroySubject.next();
    this._destroySubject.complete();
  }

  public update(): void {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.width = '500px';

    dialogConfig.data = {
      id: 1,
      title: 'Update Animal',
      edit: this.animal,
    };

    this.dialog.open<AnimalDialogComponent, IAnimalDialogData>(
      AnimalDialogComponent,
      dialogConfig
    );
  }

  public delete(): void {
    this.isDeleting = true;

    this.animalService
      .deleteAnimal(this.animal.id)
      .pipe(
        catchError((err, caught) => {
          this.snackbar.open(err);

          console.log('Hello');

          return caught;
        }),
        takeUntil(this._destroySubject)
      )
      .subscribe((animal: IAnimal) => {
        this.isDeleting = false;
      });
  }
}
