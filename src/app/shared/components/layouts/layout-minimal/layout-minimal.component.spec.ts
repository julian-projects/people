import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LayoutMinimalComponent } from '@shared/components/layouts/layout-minimal/layout-minimal.component';

describe('LayoutMinimalComponent', () => {
  let component: LayoutMinimalComponent;
  let fixture: ComponentFixture<LayoutMinimalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LayoutMinimalComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(LayoutMinimalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
